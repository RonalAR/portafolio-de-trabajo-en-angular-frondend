import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from '../app/componentes/home-page/home-page.component';
import { ProyectoComponent } from '../app/componentes/proyecto/proyecto.component';

const routes: Routes = [
  {
    path:'', component: HomePageComponent,
  },
  {
    path:'proyecto', component: ProyectoComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
